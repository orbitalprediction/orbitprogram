/* N3EMO Orbit Simulator routines  v3.7 */

/* Copyright (c) 1986,1987,1988,1989,1990 Robert W. Berger N3EMO
   May be freely distributed, provided this notice remains intact. */

#include <stdio.h>
#include <math.h>
 
#define SSPELLIPSE 0		/* If non zero, use ellipsoidal earth model
				   when calculating longitude, latitude, and
				   height */

#ifndef PI
#define PI 3.14159265
#endif

#ifdef PI2
#undef PI2
#endif

#ifdef E
#undef E
#endif

typedef double mat3x3[3][3];

#define PI2 (PI*2)
#define MinutesPerDay (24*60.0)
#define SecondsPerDay (60*MinutesPerDay)
#define HalfSecond (0.5/SecondsPerDay)
#define EarthRadius 6378.16             /* Kilometers, at equator */

#define EarthFlat (1/298.25)            /* Earth Flattening Coeff. */
#define SiderealSolar 1.0027379093
#define SidRate (PI2*SiderealSolar/SecondsPerDay)	/* radians/second */
#define GM 398600			/* Kilometers^3/seconds^2 */
#define DegreesPerRadian (180/PI)
#define RadiansPerDegree (PI/180)
#define ABS(x) ((x) < 0 ? (-(x)) : (x))
#define SQR(x) ((x)*(x))
 
#define Epsilon (RadiansPerDegree/3600)     /* 1 arc second */
#define SunRadius 695000		
#define SunSemiMajorAxis  149598845.0  	    /* Kilometers 		   */


double SidDay,SidReference;	/* Date and sidereal time	*/

/* Keplerian elements for the sun */
double SunEpochTime,SunInclination,SunRAAN,SunEccentricity,
       SunArgPerigee,SunMeanAnomaly,SunMeanMotion;

/* values for shadow geometry */
double SinPenumbra,CosPenumbra;

 
/* Solve Kepler's equation                                      */
/* Inputs:                                                      */
/*      MeanAnomaly     Time Since last perigee, in radians.    */
/*                      PI2 = one complete orbit.               */
/*      Eccentricity    Eccentricity of orbit's ellipse.        */
/* Output:                                                      */
/*      TrueAnomaly     Angle between perigee, geocenter, and   */
/*                      current position.                       */
 
long calls = 0;
long iters = 0;

dumpstats()
{
printf("Average iterations = %lf\n",((double) iters)/calls);
}

double Kepler(MeanAnomaly,Eccentricity)
register double MeanAnomaly,Eccentricity;
 
{
register double E;              /* Eccentric Anomaly                    */
register double Error;
register double TrueAnomaly;
 
calls++;

    E = MeanAnomaly ;/*+ Eccentricity*sin(MeanAnomaly);   /* Initial guess */
    do
        {
        Error = (E - Eccentricity*sin(E) - MeanAnomaly)
                / (1 - Eccentricity*cos(E));
        E -= Error;
iters++;
        }
   while (ABS(Error) >= Epsilon);

    if (ABS(E-PI) < Epsilon)
        TrueAnomaly = PI;
      else
        TrueAnomaly = 2*atan(sqrt((1+Eccentricity)/(1-Eccentricity))
                                *tan(E/2));
    if (TrueAnomaly < 0)
        TrueAnomaly += PI2;
 
    return TrueAnomaly;
}
 
GetSubSatPoint(SatX,SatY,SatZ,Time,Latitude,Longitude,Height)
double SatX,SatY,SatZ,Time;
double *Latitude,*Longitude,*Height;
{
    double r;
    long i;

    r = sqrt(SQR(SatX) + SQR(SatY) + SQR(SatZ));

    *Longitude = PI2*((Time-SidDay)*SiderealSolar + SidReference)
		    - atan2(SatY,SatX);

    /* i = floor(Longitude/2*pi)        */
    i = *Longitude/PI2;
    if(i < 0)
        i--;
 
    *Longitude -= i*PI2;

    *Latitude = atan(SatZ/sqrt(SQR(SatX) + SQR(SatY)));

#if SSPELLIPSE
#else
    *Height = r - EarthRadius;
#endif
}
 
 
GetPrecession(SemiMajorAxis,Eccentricity,Inclination,
        RAANPrecession,PerigeePrecession)
double SemiMajorAxis,Eccentricity,Inclination;
double *RAANPrecession,*PerigeePrecession;
{
  *RAANPrecession = 9.95*pow(EarthRadius/SemiMajorAxis,3.5) * cos(Inclination)
                 / SQR(1-SQR(Eccentricity)) * RadiansPerDegree;
 
  *PerigeePrecession = 4.97*pow(EarthRadius/SemiMajorAxis,3.5)
         * (5*SQR(cos(Inclination))-1)
                 / SQR(1-SQR(Eccentricity)) * RadiansPerDegree;
}
 
/* Compute the satellite postion and velocity in the RA based coordinate
   system */

GetSatPosition(EpochTime,EpochRAAN,EpochArgPerigee,SemiMajorAxis,
	Inclination,Eccentricity,RAANPrecession,PerigeePrecession,
	Time,TrueAnomaly,X,Y,Z,Radius,VX,VY,VZ)
 
double EpochTime,EpochRAAN,EpochArgPerigee;
double SemiMajorAxis,Inclination,Eccentricity;
double RAANPrecession,PerigeePrecession,Time, TrueAnomaly;
double *X,*Y,*Z,*Radius,*VX,*VY,*VZ;

{
    double RAAN,ArgPerigee;
 

    double Xw,Yw,VXw,VYw;	/* In orbital plane */
    double Tmp;
    double Px,Qx,Py,Qy,Pz,Qz;	/* Escobal transformation 31 */
    double CosArgPerigee,SinArgPerigee;
    double CosRAAN,SinRAAN,CoSinclination,SinInclination;

        *Radius = SemiMajorAxis*(1-SQR(Eccentricity))
                        / (1+Eccentricity*cos(TrueAnomaly));


    Xw = *Radius * cos(TrueAnomaly);
    Yw = *Radius * sin(TrueAnomaly);
    
    Tmp = sqrt(GM/(SemiMajorAxis*(1-SQR(Eccentricity))));

    VXw = -Tmp*sin(TrueAnomaly);
    VYw = Tmp*(cos(TrueAnomaly) + Eccentricity);

    ArgPerigee = EpochArgPerigee + (Time-EpochTime)*PerigeePrecession;
    RAAN = EpochRAAN - (Time-EpochTime)*RAANPrecession;

    CosRAAN = cos(RAAN); SinRAAN = sin(RAAN);
    CosArgPerigee = cos(ArgPerigee); SinArgPerigee = sin(ArgPerigee);
    CoSinclination = cos(Inclination); SinInclination = sin(Inclination);
    
    Px = CosArgPerigee*CosRAAN - SinArgPerigee*SinRAAN*CoSinclination;
    Py = CosArgPerigee*SinRAAN + SinArgPerigee*CosRAAN*CoSinclination;
    Pz = SinArgPerigee*SinInclination;
    Qx = -SinArgPerigee*CosRAAN - CosArgPerigee*SinRAAN*CoSinclination;
    Qy = -SinArgPerigee*SinRAAN + CosArgPerigee*CosRAAN*CoSinclination;
    Qz = CosArgPerigee*SinInclination;

    *X = Px*Xw + Qx*Yw;		/* Escobal, transformation #31 */
    *Y = Py*Xw + Qy*Yw;
    *Z = Pz*Xw + Qz*Yw;

    *VX = Px*VXw + Qx*VYw;
    *VY = Py*VXw + Qy*VYw;
    *VZ = Pz*VXw + Qz*VYw;
}

/* Compute the site postion and velocity in the RA based coordinate
   system. SiteMatrix is set to a matrix which is used by GetTopoCentric
   to convert geocentric coordinates to topocentric (observer-centered)
    coordinates. */

GetSitPosition(SiteLat,SiteLong,SiteElevation,CurrentTime,
             SiteX,SiteY,SiteZ,SiteVX,SiteVY,SiteMatrix)

double SiteLat,SiteLong,SiteElevation,CurrentTime;
double *SiteX,*SiteY,*SiteZ,*SiteVX,*SiteVY;
mat3x3 SiteMatrix;

{
    static double G1,G2; /* Used to correct for flattening of the Earth */
    static double CosLat,SinLat;
    static double OldSiteLat = -100000;  /* Used to avoid unneccesary recomputation */
    static double OldSiteElevation = -100000;
    double Lat;
    double SiteRA;	/* Right Ascension of site			*/
    double CosRA,SinRA;

    if ((SiteLat != OldSiteLat) || (SiteElevation != OldSiteElevation))
	{
	OldSiteLat = SiteLat;
	OldSiteElevation = SiteElevation;
	Lat = atan(1/(1-SQR(EarthFlat))*tan(SiteLat));

	CosLat = cos(Lat);
	SinLat = sin(Lat);

	G1 = EarthRadius/(sqrt(1-(2*EarthFlat-SQR(EarthFlat))*SQR(SinLat)));
	G2 = G1*SQR(1-EarthFlat);
	G1 += SiteElevation;
	G2 += SiteElevation;
	}


    SiteRA = PI2*((CurrentTime-SidDay)*SiderealSolar + SidReference)
	         - SiteLong;
    CosRA = cos(SiteRA);
    SinRA = sin(SiteRA);
    

    *SiteX = G1*CosLat*CosRA;
    *SiteY = G1*CosLat*SinRA;
    *SiteZ = G2*SinLat;
    *SiteVX = -SidRate * *SiteY;
    *SiteVY = SidRate * *SiteX;

    SiteMatrix[0][0] = SinLat*CosRA;
    SiteMatrix[0][1] = SinLat*SinRA;
    SiteMatrix[0][2] = -CosLat;
    SiteMatrix[1][0] = -SinRA;
    SiteMatrix[1][1] = CosRA;
    SiteMatrix[1][2] = 0.0;
    SiteMatrix[2][0] = CosRA*CosLat;
    SiteMatrix[2][1] = SinRA*CosLat;
    SiteMatrix[2][2] = SinLat;
}

GetRange(SiteX,SiteY,SiteZ,SiteVX,SiteVY,
	SatX,SatY,SatZ,SatVX,SatVY,SatVZ,Range,RangeRate)

double SiteX,SiteY,SiteZ,SiteVX,SiteVY;
double SatX,SatY,SatZ,SatVX,SatVY,SatVZ;
double *Range,*RangeRate;
{
    double DX,DY,DZ;

    DX = SatX - SiteX; DY = SatY - SiteY; DZ = SatZ - SiteZ;

    *Range = sqrt(SQR(DX)+SQR(DY)+SQR(DZ));    

    *RangeRate = ((SatVX-SiteVX)*DX + (SatVY-SiteVY)*DY + SatVZ*DZ)
			/ *Range;
}

/* Convert from geocentric RA based coordinates to topocentric
   (observer centered) coordinates */

GetTopocentric(SatX,SatY,SatZ,SiteX,SiteY,SiteZ,SiteMatrix,X,Y,Z)
double SatX,SatY,SatZ,SiteX,SiteY,SiteZ;
double *X,*Y,*Z;
mat3x3 SiteMatrix;
{
    SatX -= SiteX;
    SatY -= SiteY;
    SatZ -= SiteZ;

    *X = SiteMatrix[0][0]*SatX + SiteMatrix[0][1]*SatY
	+ SiteMatrix[0][2]*SatZ; 
    *Y = SiteMatrix[1][0]*SatX + SiteMatrix[1][1]*SatY
	+ SiteMatrix[1][2]*SatZ; 
    *Z = SiteMatrix[2][0]*SatX + SiteMatrix[2][1]*SatY
	+ SiteMatrix[2][2]*SatZ; 
}

GetBearings(SatX,SatY,SatZ,SiteX,SiteY,SiteZ,SiteMatrix,Azimuth,Elevation)
double SatX,SatY,SatZ,SiteX,SiteY,SiteZ;
mat3x3 SiteMatrix;
double *Azimuth,*Elevation;
{
    double x,y,z;

    GetTopocentric(SatX,SatY,SatZ,SiteX,SiteY,SiteZ,SiteMatrix,&x,&y,&z);

    *Elevation = atan(z/sqrt(SQR(x) + SQR(y)));

    *Azimuth = PI - atan2(y,x);

    if (*Azimuth < 0)
	*Azimuth += PI;
}

Eclipsed(SatX,SatY,SatZ,SatRadius,CurrentTime)
double SatX,SatY,SatZ,SatRadius,CurrentTime;
{
    double MeanAnomaly,TrueAnomaly;
    double SunX,SunY,SunZ,SunRad;
    double vx,vy,vz;
    double CosTheta;

    MeanAnomaly = SunMeanAnomaly+ (CurrentTime-SunEpochTime)*SunMeanMotion*PI2;
    TrueAnomaly = Kepler(MeanAnomaly,SunEccentricity);

    GetSatPosition(SunEpochTime,SunRAAN,SunArgPerigee,SunSemiMajorAxis,
		SunInclination,SunEccentricity,0.0,0.0,CurrentTime,
		TrueAnomaly,&SunX,&SunY,&SunZ,&SunRad,&vx,&vy,&vz);

    CosTheta = (SunX*SatX + SunY*SatY + SunZ*SatZ)/(SunRad*SatRadius)
		 *CosPenumbra + (SatRadius/EarthRadius)*SinPenumbra;

    if (CosTheta < 0)
        if (CosTheta < -sqrt(SQR(SatRadius)-SQR(EarthRadius))/SatRadius
	    		*CosPenumbra + (SatRadius/EarthRadius)*SinPenumbra)
	  
	    return 1;
    return 0;
}

/* Initialize the Sun's keplerian elements for a given epoch.
   Formulas are from "Explanatory Supplement to the Astronomical Ephemeris".
   Also init the sidereal reference				*/

InitOrbitRoutines(EpochDay)
double EpochDay;
{
    double T,T2,T3,Omega;
    int n;
    double SunTrueAnomaly,SunDistance;

    T = (floor(EpochDay)-0.5)/36525;
    T2 = T*T;
    T3 = T2*T;

    SidDay = floor(EpochDay);

    SidReference = (6.6460656 + 2400.051262*T + 0.00002581*T2)/24;
    SidReference -= floor(SidReference);

    /* Omega is used to correct for the nutation and the abberation */
    Omega = (259.18 - 1934.142*T) * RadiansPerDegree;
    n = Omega / PI2;
    Omega -= n*PI2;

    SunEpochTime = EpochDay;
    SunRAAN = 0;

    SunInclination = (23.452294 - 0.0130125*T - 0.00000164*T2
		    + 0.000000503*T3 +0.00256*cos(Omega)) * RadiansPerDegree;
    SunEccentricity = (0.01675104 - 0.00004180*T - 0.000000126*T2);
    SunArgPerigee = (281.220833 + 1.719175*T + 0.0004527*T2
			+ 0.0000033*T3) * RadiansPerDegree;
    SunMeanAnomaly = (358.475845 + 35999.04975*T - 0.00015*T2
			- 0.00000333333*T3) * RadiansPerDegree;
    n = SunMeanAnomaly / PI2;
    SunMeanAnomaly -= n*PI2;

    SunMeanMotion = 1/(365.24219879 - 0.00000614*T);

    SunTrueAnomaly = Kepler(SunMeanAnomaly,SunEccentricity);
    SunDistance = SunSemiMajorAxis*(1-SQR(SunEccentricity))
			/ (1+SunEccentricity*cos(SunTrueAnomaly));

    SinPenumbra = (SunRadius-EarthRadius)/SunDistance;
    CosPenumbra = sqrt(1-SQR(SinPenumbra));
}

 
SPrintTime(Str,Time)
char *Str;
double Time;
{
    int day,hours,minutes,seconds;
 
    day = Time;
    Time -= day;
    if (Time < 0)
        Time += 1.0;   /* Correct for truncation problems with negatives */
 
    hours = Time*24;
    Time -=  hours/24.0;
 
    minutes = Time*MinutesPerDay;
    Time -= minutes/MinutesPerDay;
 
    seconds = Time*SecondsPerDay;
    seconds -= seconds/SecondsPerDay;
 
    sprintf(Str,"%02d%02d:%02d",hours,minutes,seconds);
}
 
PrintTime(OutFile,Time)
FILE *OutFile;
double Time;
{
    char str[100];

    SPrintTime(str,Time);
    fprintf(OutFile,"%s",str);
}


/* Get the Day Number for a given date. January 1 of the reference year
   is day 0. Note that the Day Number may be negative, if the sidereal
   reference is in the future.                                          */
 
/* Date calculation routines
  Robert Berger @ Carnegie Mellon

  January 1, 1900 is day 0
  valid from 1900 through 2099 */

/* #include <stdio.h> */

char *MonthNames[] = { "Jan","Feb","Mar","Apr","May","Jun","Jul",
                        "Aug","Sep","Oct","Nov","Dec" };
 
int MonthDays[] = {0,31,59,90,120,151,181,212,243,273,304,334};
		
 
char *DayNames[] = { "Sunday","Monday","Tuesday","Wednesday","Thursday",
                        "Friday","Saturday"};


long GetDayNum(Year,Month,Day)
{
    long Result;
    
    /* Heuristic to allow 4 or 2 digit year specifications */
    if (Year < 50)
	Year += 2000;
      else if (Year < 100)
	Year += 1900;
	
    Result = ((((long) Year-1901)*1461)>>2) + MonthDays[Month-1] + Day + 365;
    if (Year%4 == 0 && Month > 2)
        Result++;

    return Result;
}

GetDate(DayNum,Year,Month,Day)
long DayNum;
int *Year,*Month,*Day;    
{
    int M,L;
    long Y;
	
    Y = 4*DayNum;
    Y /= 1461;

    DayNum =  DayNum -365 - (((Y-1)*1461)>>2);
    
    L = 0;
    if (Y%4 == 0 && DayNum > MonthDays[2])
        L = 1;
	
    M = 1;
	     
    while (DayNum > MonthDays[M]+L)
	M++;
	
    DayNum -= (MonthDays[M-1]);
    if (M > 2)
        DayNum -= L;
   
    *Year = Y+1900;
    *Month = M;
    *Day = DayNum;
}    

/* Sunday = 0 */
GetDayOfWeek(DayNum)
long DayNum;
{
    return DayNum % 7;
}    

SPrintDate(Str,DayNum)
char *Str;
long DayNum;
{
    int Month,Day,Year;
    
    GetDate(DayNum,&Year,&Month,&Day);
    sprintf(Str,"%d %s %d",Day,
                MonthNames[Month-1],Year);
} 

SPrintDayOfWeek(Str,DayNum)
char *Str;
long DayNum;
{
    strcpy(Str,DayNames[DayNum%7]);
}

PrintDate(OutFile,DayNum)
FILE *OutFile;
long DayNum;
{
    char str[100];

    SPrintDate(str,DayNum);
    fprintf(OutFile,"%s",str);
}

PrintDayOfWeek(OutFile,DayNum)
FILE *OutFile;
long DayNum;
{
    fprintf(OutFile,"%s",DayNames[DayNum%7]);
}    
  
/* Copyright (c) 1986,1987,1988,1989,1990 Robert W. Berger N3EMO
   May be freely distributed, provided this notice remains intact. */

/* Change Log
	4/2/1990	v3.9 Misc bug fixes. Changed ElementSet and
			EpochRev to unsigned longs in nasa.c. Allow
			satellite names with spaces in mode.dat.

	3/15/1990	v3.8 Stop assigning single character abbreviations
			after the 62nd satellite.

	3/7/1990	v3.7 Make Phase III style phase (0-255) the default.
			Ignore case in satellite names.

	12/19/1989	v3.6 Use more direct calculations for dates.
			Calculate a new sidereal time reference for each run.

	12/8/1988	v3.5 Allow multiple overlapping modes in "mode.dat".

	6/28/1988	v3.4 Cleaned up Eclipse code. Fixed leap year handling
			for centesimal years. Added a heuristic to GetDay to
			allow 2 or 4 digit year specifications.
				  
	1/25/1988	v3.2 Rewrote orbitr.c to improve modularity,
			efficiency, and accuracy. Adopted geocentric
			cartesian coordinates as the standard representation
			for position and velocity. Added direct calculation
			 of range-rate for better doppler predections.

	12/1/1988	v3.1 Allow spaces in satellite names. Provide
			single character aliases for 62 satellites 
			(up from 26).

	4/7/87		v3.0 Added eclipses.

	4/1/87		v2.4 Added "Flip" option in site file for
			0-180 elevation support.

	3/24/87		v2.3 Adapted for new kepler.dat format.
			Allow beacon frequencies in mode.dat.
			Use decay rate for drag compensation.

	5/10/86		v2.2 Added single character aliases for satellite
			names.

	4/30/86		v2.1 Print blank line if satellite dips below
			horizon and reappears during same orbit and day

	4/29/86		v2.0 Changed GetSatelliteParams() to use AMSAT's
			"kepler.dat" file. Moved schedule to "mode.dat" file.

        4/22/86         v1.3  Inserted N8FJB's suggestions for variable naming
                        which maintain 8 character uniqueness.
                        Also removed "include" file orbitr.h, which had two
                        definitions of external functions defined in orbit.c
			    -K3MC 

        4/1/86          v1.2  Corrected a scanf conversion to %d for an int
                        type.    -K3MC
 
        3/19/86         v1.1  Changed GetSatelliteParams to not pass NULL
                        to sscanf.
                                                                        */
 
#define DRAG 1

#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <string.h> /*mac*//*23 Jul 99*/
#include <stdlib.h> /*mac*//*23 Jul 99*//* Added for MSVC 1.52c*/

extern double Kepler();
extern long GetDayNum();
 
#define LC(c) (isupper(c) ? tolower(c) : (c))

#ifndef PI
#define PI 3.14159265
#endif

#ifdef PI2
#undef PI2
#endif

#define PI2 (PI*2)

#define MinutesPerDay (24*60.0)
#define SecondsPerDay (60*MinutesPerDay)
#define HalfSecond (0.5/SecondsPerDay)
#define EarthRadius 6378.16             /* Kilometers           */
#define C 2.997925e5                    /* Kilometers/Second    */
#define TropicalYear 365.24199		/* Mean solar days	*/
#define EarthEccentricity 0.016713
#define DegreesPerRadian (180/PI)
#define RadiansPerDegree (PI/180)
#define ABS(x) ((x) < 0 ? (-(x)) : (x))
#define SQR(x) ((x)*(x))
 
#define MaxModes 10
typedef struct {
                int MinPhase,MaxPhase;
                char ModeStr[20];
               }  ModeRec;
 
char VersionStr[] = "N3EMO Orbit Simulator  v3.9";
 
    /*  Keplerian Elements and misc. data for the satellite              */
    double  EpochDay;                   /* time of epoch                 */
    double EpochMeanAnomaly;            /* Mean Anomaly at epoch         */
    long EpochOrbitNum;                 /* Integer orbit # of epoch      */
    double EpochRAAN;                   /* RAAN at epoch                 */
    double epochMeanMotion;             /* Revolutions/day               */
    double OrbitalDecay;                /* Revolutions/day^2             */
    double EpochArgPerigee;             /* argument of perigee at epoch  */
    double Eccentricity;
    double Inclination;
    char SatName[100];
    int ElementSet;
    double BeaconFreq;                  /* Mhz, used for doppler calc    */
    double MaxPhase;                    /* Phase units in 1 orbit        */
    double perigeePhase;
    int NumModes;
    ModeRec Modes[MaxModes];
    int PrintApogee;
    int PrintEclipses;
    int Flip;
 
    /* Simulation Parameters */
 
    double StartTime,EndTime, StepTime; /* In Days, 1 = New Year        */
                                        /*      of reference year       */
 
    /* Site Parameters */
    char SiteName[100];
    double SiteLat,SiteLong,SiteAltitude,SiteMinElev;
 
 
/* List the satellites in kepler.dat, and return the number found */
ListSatellites()
{
    char str[100];
    FILE *InFile;
    char satchar;
    int NumSatellites;

    printf("Available satellites:\n");

    if ((InFile = fopen("kepler.dat","r")) == 0)
        {
	printf("\"kepler.dat\" not found\n");
	exit(-1);
	}

    satchar = 'a';
    NumSatellites = 0;
    while (fgets(str,100,InFile))
	if (strncmp(str,"Satellite: ",11) == 0)
	    {
	    printf("\t");
	    if (satchar != 0)
		printf("%c) ",satchar);
	     else
		printf("   ");
	    printf("%s",&str[11]);
	    if (satchar)
		{
		if (satchar == 'z')
		    satchar = 'A';
                   else if (satchar == 'Z')
		      satchar = '0';
		     else if (satchar == '9')
		       satchar = 0;
	    	         else satchar++;
		}
	    NumSatellites++;
	    }

    fclose(InFile);

    return NumSatellites;
}

/* Match and skip over a string in the input file. Exits on failure. */

MatchStr(InFile,FileName,Target)
FILE *InFile;
char *FileName,*Target;
{
    char str[100];

    fgets(str,strlen(Target)+1,InFile);
    if (strcmp(Target,str))
       {
       printf("%s: found \"%s\" while expecting \"%s\n\"",FileName,str,Target);
       exit(-1);
       }
}

LetterNum(c)
char c;
{
    if (c >= 'a' && c <= 'z')
	return c - 'a' + 1;
      else if (c >= 'A' && c <= 'Z')
 	  return c - 'A'+ 27;
	else if (c >= '0' && c <= '9')
	  return c - '0' + 53;
}
      
/* Case insensitive strncmp */
cstrncmp(str1,str2,l)
char *str1,*str2;
{
    int i;

    for (i = 0; i < l; i++)
	if (LC(str1[i]) != LC(str2[i]))
	    return 1;

    return 0;
}


cstrcmp(str1,str2)
char *str1,*str2;
{
    int i,l;

    l = strlen(str1);
    if (strlen(str2) != l)
	return 1;

    for (i = 0; i < l; i++)
	if (LC(str1[i]) != LC(str2[i]))
	    return 1;

    return 0;
}

void
GetSatelliteParams()
{
    FILE *InFile;
    char str[100];
    int EpochYear;
    double EpochHour,EpochMinute,EpochSecond;
    int found;
    int i,NumSatellites;
    char satchar;

    NumSatellites = ListSatellites();

    found = 0;

    while (!found)
	{
	printf("Letter or satellite name :");
	gets(SatName);

	if ((InFile = fopen("kepler.dat","r")) == 0)
	    {
	    printf("kepler.dat not found\n");
	    exit(-1);
	    }

	if (strlen(SatName) == 1)
	    {			/* use single character label */
	    satchar = SatName[0];
	    if (LetterNum(satchar) > NumSatellites)
	        {
	    	printf("'%c' is out of range\n",satchar);
		fclose(InFile);
		continue;
		}

	    for (i = 1; i <= LetterNum(satchar); i++)
		{
		do  /* find line beginning with "Satellite: " */
		    fgets(str,100,InFile);
		while (strncmp(str,"Satellite: ",11) != 0);
		}
	    found = 1;
	    strncpy(SatName,&str[11],strlen(str)-12);
	    }
		
	 else 
	     {
	     while (!found)  /* use satellite name */
            	{
	    	if (! fgets(str,100,InFile))
	    	    break;	/* EOF */

	    	if (strncmp(str,"Satellite: ",11) == 0)
		   if (cstrncmp(SatName,&str[11],strlen(SatName)) == 0)
			found = 1;
	        }

	    if (!found)
		{
		printf("Satellite %s not found\n",SatName);
		fclose(InFile);
		}
	    }
	}

    BeaconFreq = 146.0;  /* Default value */

    fgets(str,100,InFile);	/* Skip line */

    MatchStr(InFile,"kepler.dat","Epoch time:");
    fgets(str,100,InFile);
    sscanf(str,"%lf",&EpochDay);

    EpochYear = EpochDay / 1000.0;
    EpochDay -= EpochYear*1000.0;
    EpochDay += GetDayNum(EpochYear,1,0);
    fgets(str,100,InFile);

    if (sscanf(str,"Element set: %ld",&ElementSet) == 0)
       {   /* Old style kepler.dat */
       MatchStr(InFile,"kepler.dat","Element set:");
       fgets(str,100,InFile);
       sscanf(str,"%d",&ElementSet);
       }

    MatchStr(InFile,"kepler.dat","Inclination:");
    fgets(str,100,InFile);
    sscanf(str,"%lf",&Inclination);
    Inclination *= RadiansPerDegree;

    MatchStr(InFile,"kepler.dat","RA of node:");
    fgets(str,100,InFile);
    sscanf(str,"%lf",&EpochRAAN);
    EpochRAAN *= RadiansPerDegree;

    MatchStr(InFile,"kepler.dat","Eccentricity:");
    fgets(str,100,InFile);
    sscanf(str,"%lf",&Eccentricity);

    MatchStr(InFile,"kepler.dat","Arg of perigee:");
    fgets(str,100,InFile);
    sscanf(str,"%lf",&EpochArgPerigee);
    EpochArgPerigee *= RadiansPerDegree;

    MatchStr(InFile,"kepler.dat","Mean anomaly:");
    fgets(str,100,InFile);
    sscanf(str,"%lf",&EpochMeanAnomaly);
    EpochMeanAnomaly *= RadiansPerDegree;

    MatchStr(InFile,"kepler.dat","Mean motion:");
    fgets(str,100,InFile);
    sscanf(str,"%lf",&epochMeanMotion);

    MatchStr(InFile,"kepler.dat","Decay rate:");
    fgets(str,100,InFile);
    sscanf(str,"%lf",&OrbitalDecay);

    MatchStr(InFile,"kepler.dat","Epoch rev:");
    fgets(str,100,InFile);
    sscanf(str,"%ld",&EpochOrbitNum);

	while (1)
	    {
	    if (! fgets(str,100,InFile))
		break;	/* EOF */
	    if (strlen(str) <= 2)
	    	break;  /* Blank line */
	    sscanf(str,"Beacon: %lf",&BeaconFreq);
	    }

    PrintApogee = (Eccentricity >= 0.3);

    perigeePhase = 0; MaxPhase = 256; /* Default values */
    NumModes = 0;

    if ((InFile = fopen("mode.dat","r")) == 0)
	return;

    found = 0;
    while (!found)
        {
	if (! fgets(str,100,InFile))
	    break;	/* EOF */
 	if (strncmp(str,"Satellite: ",11) == 0)
 	  if (cstrncmp(SatName,&str[11],strlen(SatName)) == 0)
		found = 1;
	}
	
    if (found)
	{
	while (1)
	    {
	    if (! fgets(str,100,InFile))
		break;	/* EOF */
	    if (strlen(str) <= 2)
	    	break;  /* Blank line */
	    sscanf(str,"Beacon: %lf",&BeaconFreq);
	    sscanf(str,"Perigee phase: %lf",&perigeePhase);
	    sscanf(str,"Max phase: %lf",&MaxPhase);

	    if (sscanf(str,"Mode: %20s from %d to %d",Modes[NumModes].ModeStr,
	    &Modes[NumModes].MinPhase,&Modes[NumModes].MaxPhase) == 3
	      && NumModes < MaxModes)
		  NumModes++;
	    }
	fclose(InFile);
	}
}

 
GetSiteParams()
{
    FILE *InFile;
    char name[100],str[100];
 
    printf("Site name :");
    gets(name);
    strcat(name,".sit");
 
    if ((InFile = fopen(name,"r")) == 0)
        {
        printf("%s not found\n",name);
        exit(-1);
        }
 
    fgets(SiteName,100,InFile);
 
    fgets(str,100,InFile);
    sscanf(str,"%lf",&SiteLat);
    SiteLat *= RadiansPerDegree;
 
    fgets(str,100,InFile);
    sscanf(str,"%lf",&SiteLong);
    SiteLong *= RadiansPerDegree;
 
    fgets(str,100,InFile);
    sscanf(str,"%lf",&SiteAltitude);
    SiteAltitude /= 1000;   /* convert to km */
 
    fgets(str,100,InFile);
    sscanf(str,"%lf",&SiteMinElev);
    SiteMinElev *= RadiansPerDegree;

    Flip = PrintEclipses = 0;
    while (fgets(str,100,InFile))
	{
	if (strncmp(str,"Flip",4) == 0)
	    Flip = 1;
	  else if (strncmp(str,"Eclipse",7) == 0)
	    PrintEclipses = 1;
	   else printf("\"%s\" unknown option: %s",name,str);
	}
}
 
GetSimulationParams()
{
    double hour,duration;
    int Month,Day,Year;
 
again:
    printf("Start date (UTC) (Month Day Year) :");
    scanf("%d%d%d",&Month,&Day,&Year);
    if(Year < 2000) goto again;
 
    StartTime = GetDayNum(Year,Month,Day);
    printf("Starting Hour (UTC) :");
    scanf("%lf",&hour);
    StartTime += hour/24;
 
    printf("Duration (Days) :");
    scanf("%lf",&duration);
    EndTime = StartTime + duration;
 
    printf("Time Step (Minutes) :");
    scanf("%lf",&StepTime);
    StepTime /= MinutesPerDay;
}
 
PrintMode(OutFile,Phase)
FILE *OutFile;
{
    int CurMode;
 
    for (CurMode = 0; CurMode < NumModes; CurMode++)
        if ((Phase >= Modes[CurMode].MinPhase
                && Phase < Modes[CurMode].MaxPhase)
              || ((Modes[CurMode].MinPhase > Modes[CurMode].MaxPhase)
                  && (Phase >= Modes[CurMode].MinPhase
                        || Phase < Modes[CurMode].MaxPhase)))
            {
            fprintf(OutFile,"%s ",Modes[CurMode].ModeStr);
            }
}
 
 
main()
{
    double ReferenceOrbit;      /* Floating point orbit # at epoch */
    double CurrentTime;         /* In Days                         */
    double CurrentOrbit;
    double AverageMotion,       /* Corrected for drag              */
        CurrentMotion;
    double MeanAnomaly,TrueAnomaly;
    double SemiMajorAxis;
    double Radius;              /* From geocenter                  */
    double SatX,SatY,SatZ;	/* In Right Ascension based system */
    double SatVX,SatVY,SatVZ;   /* Kilometers/second		   */
    double SiteX,SiteY,SiteZ;
    double SiteVX,SiteVY;
    double SiteMatrix[3][3];
    double Height;
    double RAANPrecession,PerigeePrecession;
    double SSPLat,SSPLong;
    long OrbitNum,PrevOrbitNum;
    long Day,PrevDay;
    double Azimuth,Elevation,Range;
    double RangeRate,Doppler;
    int Phase;
    char FileName[100];
    FILE *OutFile;
    int DidApogee;
    double TmpTime,PrevTime;
    int PrevVisible;

    printf("%s\n",VersionStr);
 

    GetSatelliteParams();
    GetSiteParams();
    GetSimulationParams();
 
    InitOrbitRoutines((StartTime+EndTime)/2);

    printf("Output file (RETURN for TTY) :");
    gets(FileName);     /* Skip previous RETURN */
    gets(FileName);
 
 
    if (strlen(FileName) > 0)
        {
        if ((OutFile = fopen(FileName,"w")) == 0)
            {
            printf("Can't write to %s\n",FileName);
            exit(-1);
            }
        }
      else OutFile = stdout;
 
    fprintf(OutFile,"%s Element Set %d\n",SatName,ElementSet);

    fprintf(OutFile,"%s\n",SiteName);
 
    fprintf(OutFile,"Doppler calculated for freq = %lf MHz\n",BeaconFreq);
 
    SemiMajorAxis = 331.25 * exp(2*log(MinutesPerDay/epochMeanMotion)/3);
    GetPrecession(SemiMajorAxis,Eccentricity,Inclination,&RAANPrecession,
                        &PerigeePrecession);

    ReferenceOrbit = EpochMeanAnomaly/PI2 + EpochOrbitNum;
 
    PrevDay = -10000; PrevOrbitNum = -10000;
    PrevTime = StartTime-2*StepTime;
 
    BeaconFreq *= 1E6;          /* Convert to Hz */
 
    DidApogee = 0;
 
    for (CurrentTime = StartTime; CurrentTime <= EndTime;
                CurrentTime += StepTime)
        {
 
        AverageMotion = epochMeanMotion
	   + (CurrentTime-EpochDay)*OrbitalDecay/2;
        CurrentMotion = epochMeanMotion
	   + (CurrentTime-EpochDay)*OrbitalDecay;

        SemiMajorAxis = 331.25 * exp(2*log(MinutesPerDay/CurrentMotion)/3);
 
        CurrentOrbit = ReferenceOrbit +
                        (CurrentTime-EpochDay)*AverageMotion;
        OrbitNum = CurrentOrbit;
 
        MeanAnomaly = (CurrentOrbit-OrbitNum)*PI2;
 
        TmpTime = CurrentTime;
        if (MeanAnomaly < PI)
            DidApogee = 0;
        if (PrintApogee && !DidApogee && MeanAnomaly > PI)
            {                   /* Calculate Apogee */
            TmpTime -= StepTime;   /* So we pick up later where we left off */
            MeanAnomaly = PI;
            CurrentTime=EpochDay+(OrbitNum-ReferenceOrbit+0.5)/AverageMotion;
            }
 
        TrueAnomaly = Kepler(MeanAnomaly,Eccentricity);

	GetSatPosition(EpochDay,EpochRAAN,EpochArgPerigee,SemiMajorAxis,
	    Inclination,Eccentricity,RAANPrecession,PerigeePrecession,
	    CurrentTime,TrueAnomaly,&SatX,&SatY,&SatZ,&Radius,
	    &SatVX,&SatVY,&SatVZ);

	GetSitPosition(SiteLat,SiteLong,SiteAltitude,CurrentTime,
		&SiteX,&SiteY,&SiteZ,&SiteVX,&SiteVY,SiteMatrix);


	GetBearings(SatX,SatY,SatZ,SiteX,SiteY,SiteZ,SiteMatrix,
		&Azimuth,&Elevation);

 
        if (Elevation >= SiteMinElev && CurrentTime >= StartTime)
            {

            Day = CurrentTime + HalfSecond;
            if (((double) Day) > CurrentTime+HalfSecond)
                Day -= 1;    /* Correct for truncation of negative values */

	    if (OrbitNum == PrevOrbitNum && Day == PrevDay && !PrevVisible)
	    	fprintf(OutFile,"\n");	/* Dipped out of sight; print blank */

            if (OrbitNum != PrevOrbitNum || Day != PrevDay)
                {                       /* Print Header */
		PrintDayOfWeek(OutFile,(long) Day);
		fprintf(OutFile," ");
                PrintDate(OutFile,(long) Day);
                fprintf(OutFile,"  ----Orbit # %ld-----\n",OrbitNum);
                fprintf(OutFile," U.T.C.   Az  El  ");
		if (Flip)
                    fprintf(OutFile," Az'  El' ");

		fprintf(OutFile,"Doppler Range");
                fprintf(OutFile," Height  Lat  Long  Phase(%3.0lf)\n",
                                MaxPhase);
                }
            PrevOrbitNum = OrbitNum; PrevDay = Day;
            PrintTime(OutFile,CurrentTime + HalfSecond);
 
            fprintf(OutFile,"  %3.0lf %3.0lf",Azimuth*DegreesPerRadian,
                Elevation*DegreesPerRadian);
	    if (Flip)
		{
		Azimuth += PI;
		if (Azimuth >= PI2)
		    Azimuth -= PI2;
		Elevation = PI-Elevation;
		fprintf(OutFile,"  %3.0lf  %3.0lf",Azimuth*DegreesPerRadian,
			Elevation*DegreesPerRadian);
		}

  	    GetRange(SiteX,SiteY,SiteZ,SiteVX,SiteVY,
		SatX,SatY,SatZ,SatVX,SatVY,SatVZ,&Range,&RangeRate);
            Doppler = -BeaconFreq*RangeRate/C;
            fprintf(OutFile,"  %6.0lf %6.0lf",Doppler,Range);
 
	    GetSubSatPoint(SatX,SatY,SatZ,CurrentTime,
	        &SSPLat,&SSPLong,&Height);
            fprintf(OutFile," %6.0lf  %3.0lf  %4.0lf",
                Height,SSPLat*DegreesPerRadian,
                SSPLong*DegreesPerRadian);
 
            Phase = (MeanAnomaly/PI2*MaxPhase + perigeePhase);
            while (Phase < 0)
                Phase += MaxPhase;
            while (Phase >= MaxPhase)
                Phase -= MaxPhase;
 
            fprintf(OutFile," %4d  ", Phase);
            PrintMode(OutFile,Phase);

            if (PrintApogee && (MeanAnomaly == PI))
                fprintf(OutFile,"    Apogee");

	    if (PrintEclipses)
		if (Eclipsed(SatX,SatY,SatZ,Radius,CurrentTime))
		    fprintf(OutFile,"  Eclipse");

            fprintf(OutFile,"\n");
	    PrevVisible = 1;
            }
	 else
	    PrevVisible = 0;	
        if (PrintApogee && (MeanAnomaly == PI))
            DidApogee = 1;


        PrevTime = CurrentTime;
        CurrentTime = TmpTime;
        }
    dumpstats();
    fclose(OutFile);
}
